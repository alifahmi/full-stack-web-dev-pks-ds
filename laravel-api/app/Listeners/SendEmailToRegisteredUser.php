<?php

namespace App\Listeners;

use App\Mail\RegisteredUserMail;
use App\Events\UserRegisterEvent;
use Illuminate\Support\Facades\Mail;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class SendEmailToRegisteredUser implements ShouldQueue
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  UserRegisterEvent  $event
     * @return void
     */
    public function handle(UserRegisterEvent $event)
    {
        // dd($event->otp_code->user->email . " : " . $event->otp_code );
        Mail::to($event->otp_code->user->email)->send(new RegisteredUserMail($event->otp_code));
    }
}
