<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>
    Selamat datang di Web Service PKS DS.<br>
    Ini adalah kode OTP Anda: {{ $otp_code->otp }}.<br>
    Kode OTP ini berlaku 5 menit.<br>
    Jangan berikan kode ini kepada siapapun.
</body>
</html>