<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>
    Saudara {{ $comment->user->name }}, komentar Anda pada artikel berjudul "{{ $comment->post->title }}" yang ditulis oleh {{ $comment->post->user->name }} berhasil dikirim: "{{ $comment->content }}".
</body>
</html>