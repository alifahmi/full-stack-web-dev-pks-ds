// soal 1
const hitungKelilingPersegiPanjang = (panjang, lebar) => 
{
    return 2 * (panjang + lebar);
}

const hitungLuasPersegiPanjang = (panjang, lebar) => panjang * lebar;


let p = 6;
let l = 4;

console.log(hitungKelilingPersegiPanjang(p, l))
console.log(hitungLuasPersegiPanjang(p, l))
console.log()


// soal 2
const newFunction = (firstName, lastName) =>
{
    return {
        firstName,
        lastName,
        fullName (){
            console.log(firstName + " " + lastName)
        }
    }
}
   
//Driver Code 
newFunction("William", "Imoh").fullName()
console.log()


// soal 3
const newObject = {
    firstName: "Muhammad",
    lastName: "Iqbal Mubarok",
    address: "Jalan Ranamanyar",
    hobby: "playing football",
}

const {firstName, lastName, address, hobby} = newObject;

// Driver code
console.log(firstName, lastName, address, hobby)
console.log()


// soal 4
const west = ["Will", "Chris", "Sam", "Holly"]
const east = ["Gill", "Brian", "Noel", "Maggie"]
let combined = [...west, ...east]

//Driver Code
console.log(combined)
console.log()


// soal 5
const planet = "earth" 
const view = "glass" 
var before = 'Lorem ' + view + ' dolor sit amet, ' + 'consectetur adipiscing elit, ' + planet
const after = `Lorem ${view} dolor sit amet, consectetur adipiscing elit, ${planet}`

console.log(before)
console.log(after)