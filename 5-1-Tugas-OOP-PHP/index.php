<?php
// Main Classa
trait Hewan
{
    public $nama;
    public $darah = 50;
    public $jumlahKaki;
    public $keahlian;

    public function atraksi()
    {
        echo $this->nama . " sedang " . $this->keahlian;
    }
}

trait Fight
{
    public $attackPower;
    public $defencePower;

    // public $penyerang = $this->nama;
    // $sasaran = 

    public function serang($sasaran)
    {
        $penyerang = $this->nama;
        
        echo "$penyerang sedang menyerang $sasaran->nama";
        self::diserang($this, $sasaran);
    }

    private function diserang($penyerang, $sasaran)
    {
        echo "<br>$sasaran->nama sedang diserang";
        $darahsekarang = $sasaran->darah;
        $sasaran->darah = $darahsekarang - ($penyerang->attackPower / $sasaran->defencePower);
        echo "<br>darah $sasaran->nama = " . $sasaran->darah;
        echo "<br>darah $penyerang->nama = " . $penyerang->darah;
    }
}

 
trait InfoHewan 
{
    public function getInfoHewan()
    {
        echo "<b>INFO HEWAN</b>";
        echo "<br>Nama = " . $this->nama;
        echo "<br>Darah = " . $this->darah;
        echo "<br>Jumlah kaki = " . $this->jumlahKaki;
        echo "<br>Keahlian = " . $this->keahlian;
        echo "<br>Attack Power = " . $this->attackPower;
        echo "<br>Defence Power = " . $this->defencePower;
        echo "<br>Jenis hewan: " . get_class();
    }
}

// Child class
class Elang
{
    use Hewan;
    use Fight;
    use InfoHewan;
    // public $classname = Elang::class;
    
}

$elang = new Elang();
$elang->nama = "elang_1";
$elang->jumlahKaki = 2;
$elang->keahlian = "terbang tinggi";
$elang->attackPower = 10;
$elang->defencePower = 5;

class Harimau
{
    use Hewan;
    use Fight;
    use InfoHewan;

    // public $classname = Harima
}

$harimau = new Harimau();
$harimau->nama = "harimau_2";
$harimau->jumlahKaki = 4;
$harimau->keahlian = "lari cepat";
$harimau->attackPower = 7;
$harimau->defencePower = 8;

$elang->getInfoHewan();
echo "<br><br>";
$harimau->getInfoHewan();
echo "<br><br>";

$elang->atraksi();
echo "<br><br>";

$harimau->atraksi();
echo "<br><br>";

$elang->serang($harimau);
echo "<br><br>";

$harimau->serang($elang);
echo "<br><br>";

$elang->serang($harimau);
echo "<br><br>";

$elang->serang($harimau);
echo "<br><br>";

$harimau->serang($elang);
echo "<br><br>";

$elang->serang($harimau);
echo "<br><br>";

$elang->getInfoHewan();
echo "<br><br>";
$harimau->getInfoHewan();